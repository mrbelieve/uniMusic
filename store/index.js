import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		name:'cheng'
	},
    mutations: {
		CHANGE_NAME(state,name){
			state.name=name
		}
	},
    actions: {
		
	}
})
export default store