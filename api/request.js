// 龙玺文 11.22
import URLS from './urls.js'
const install = (Vue, vm) => {
	// 获取轮播图
	let getBanner = (params = {}) => vm.$u.get(URLS.banner, params);
	// 获取推荐歌单
	let getSongList = (params = {}) => vm.$u.get(URLS.songList, params);
	// 推荐新歌
	let getNewSong = (params = {}) => vm.$u.get(URLS.newSong, params);
	
	vm.$u.api = {
		getBanner,
		getSongList,
		getNewSong
	};
}

export default {
	install
}
