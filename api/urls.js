// 龙玺文 11.23
export default {
	banner: '/banner', // 轮播图
	songList: '/personalized', // 推荐歌单
	newSong: '/personalized/newsong', // 推荐新歌
	hotSinger: '/top/artists', // 热门歌手
}
