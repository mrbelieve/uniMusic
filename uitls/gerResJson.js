function getJson(str){
	let data = str.replace('<!--KG_TAG_RES_END-->', '');
	data = data.replace("<!--KG_TAG_RES_START-->","");
	return JSON.parse(data)
}
export default {
	getJson
}