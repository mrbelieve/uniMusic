import Vue from 'vue'
import App from './App'
import GoEasy from './goeasy-1.2.1';


import uView from "uview-ui";
Vue.use(uView);

import store from "./store/index.js"

// 关闭生产提示
Vue.config.productionTip = false

App.mpType = 'app'
//挂载
const app = new Vue({
	...App,
	store
})


import Interceptor from "./api/Interceptor.js"
Vue.use(Interceptor, app)

// http接口API集中管理(龙玺文 11.23)
import request from './api/request.js'
Vue.use(request, app)

Vue.prototype.$goEasy = new GoEasy({
host: "hangzhou.goeasy.io",
appkey: "BC-87ce9e8c4e0e415295c9ea9c61747ee1", //替换为您的应用appkey
    onConnected: function() {
      console.log('socket-连接成功！')
    },
    onDisconnected: function() {
      console.log('socket-连接断开！')
    },
    onConnectFailed: function(error) {
      console.log('socket-连接失败或错误！')
    }
});

app.$mount()
